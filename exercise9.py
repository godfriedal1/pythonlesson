# Generate a random number between 0 and 9, ask the user to guess which number that was and tell them whether it is too low, too high or correct one
import random
#tebakan=int(input("Guess a number between 0 and 9"))
jawaban = random.randint(0,10)
rotasi=True
count=0
berhenti=0
while (rotasi):
  if berhenti > 0:
     print "\n"
     setop=raw_input("Print Exit to exit the game or Enter to continue")
     if setop == 'Exit':
          break 
  print "\n"
  tebakan=int(input("Guess a number between 0 and 9 or Exit to quit : "))
  print "\n"
  if tebakan == jawaban:
     count += 1
     print "\n"
     print "Tebakan tepat"
     print "Number of Guess =", count
     print "\n"
     rotasi=False
     berhenti=1
  elif tebakan < jawaban:
     count += 1
     print "The guess is lower than answer"
     rotasi=True
     berhenti=1
  elif tebakan > jawaban:
     count += 1
     print "The guess is higher than the answer"
     rotasi=True
     berhenti=1

