# Ask a user to fill in a string and check whether the string is a palindrome or not

Character = raw_input('Provide a string to check : ')

# To check a palindrome or not
if Character == Character[::-1]:
   print ("kata {} is a palindrome.".format(Character))
else:
   print ("kata {} isn't a palindrome.".format(Character))

